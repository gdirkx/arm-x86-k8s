#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t'
cd `dirname $0`/..

## Performed on a CERN OpenStack VM
## CentOS Linux 7 (Core)
## Linux silverwing.cern.ch 3.10.0-957.el7.x86_64 #1 SMP Thu Nov 8 23:39:32 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux

## warning: this uses etcd with only one node (master node), if run in production, use an external etcd cluster or a db adapter

yum update

# install Docker
yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager \
  --add-repo \
  https://download.docker.com/linux/centos/docker-ce.repo

yum update && yum install -y docker-ce
mkdir /etc/docker
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "storage-opts": [
    "overlay2.override_kernel_check=true"
  ]
}
EOF

mkdir -p /etc/systemd/system/docker.service.d
systemctl daemon-reload && systemctl enable docker && systemctl restart docker

# install kubernetes server components
# consider stealing from rock64.sh for manual install instead of this repo stuff
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kube*
EOF
yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
systemctl enable --now kubelet
cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system
systemctl disable firewalld && systemctl stop firewalld

# initialise cluster
kubeadm init --pod-network-cidr=192.168.0.0/16

## make note of the join command!
## kubeadm join 188.185.67.117:6443 --token btb2pl.idy9kjw9zytwixnf --discovery-token-ca-cert-hash sha256:38fda5411bc058abe2164a2770af0a7a0f09bec2b3065fd986cb2174e0d9da4d
## after 24h -> kubeadm token create

# calico
curl \
https://docs.projectcalico.org/v3.4/getting-started/kubernetes/installation/hosted/kubernetes-datastore/calico-networking/1.7/calico.yaml \
-O
kubectl apply -f calico.yaml

# fix hybrid master vs workers
# https://github.com/kubernetes/kubeadm/issues/163#issuecomment-403337025
kubectl create -f kube-proxy-arm.yml
kubectl create -f kube-proxy-arm64.yml
kubectl create -f kube-proxy-amd64.yml
kubectl delete daemonset kube-proxy --namespace kube-system