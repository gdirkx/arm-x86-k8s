#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t'
cd `dirname $0`/..

## performed on a raspberry pi 3+
## Arch Linux ARM
## Linux pi 5.1.3-1-ARCH #1 SMP Tue May 21 00:21:28 UTC 2019 aarch64 GNU/Linux
## https://gist.github.com/theramiyer/cb2b406128e54faa12c37e1a01f7ae15

# fetched from https://github.com/kubernetes/kubernetes/releases > CHANGELOG > binaries
curl -OL https://dl.k8s.io/v1.14.2/kubernetes-node-linux-arm64.tar.gz
tar xf kubernetes-node-linux-arm64.tar.gz
mkdir kubernetes/kubernetes-src
tar xf kubernetes/kubernetes-src.tar.gz -C kubernetes/kubernetes-src
ls kubernetes/node/bin | grep -v '\.' | xargs -n1 -I {} ln -s `pwd`/kubernetes/node/bin/{} /usr/bin/{}
cp kubernetes/kubernetes-src/build/rpms/kubelet.service /lib/systemd/system/kubelet.service
mkdir -p /etc/systemd/system/kubelet.service.d/
cp kubernetes/kubernetes-src/build/rpms/*-kubeadm.conf /etc/systemd/system/kubelet.service.d/
systemctl daemon-reload
systemctl enable kubelet

# set docker backend to systemd
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF